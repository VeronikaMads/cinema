package com.example.cinema.mapper;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.dto.MarathonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface MarathonMapper {
    Marathon map(MarathonDto dto);
}
