package com.example.cinema.mapper;

import com.example.cinema.entity.domain.Ticket;
import com.example.cinema.entity.domain.dto.TicketDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface TicketMapper {
    Ticket map(TicketDto dto);
}
