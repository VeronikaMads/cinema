package com.example.cinema.mapper;

import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.Ticket;
import com.example.cinema.entity.domain.dto.SessionDto;
import com.example.cinema.entity.domain.dto.TicketDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface SessionMapper {
    Session map(SessionDto dto);
}
