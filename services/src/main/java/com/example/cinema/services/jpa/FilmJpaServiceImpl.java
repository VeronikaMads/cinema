package com.example.cinema.services.jpa;

import com.example.cinema.entity.domain.Film;
import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Poster;
import com.example.cinema.entity.enumEntity.GenreType;
import com.example.cinema.repository.FilmRepository;
import com.example.cinema.repository.MarathonRepository;
import com.example.cinema.repository.PosterRepository;
import com.example.cinema.services.FilmService;
import com.example.cinema.services.MarathonService;
import com.example.cinema.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class FilmJpaServiceImpl extends AbstractJpaService<Film, Long> implements FilmService {
    private final FilmRepository filmRepository;
    private final MarathonRepository marathonRepository;
    private final MarathonService marathonService;
    private final PosterRepository posterRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public Long createFilm(String name, String description, GenreType genreType, Double length, Integer requiredAge, String posterFilePath) {
        Film film = Film.builder()
                .id(null)
                .name(name)
                .description(description)
                .genreType(genreType)
                .length(length)
                .requiredAge(requiredAge)
                .build();
        filmRepository.save(film);
        if (posterFilePath!=null){
            createPoster(film, posterFilePath);
        }
        return film.getId();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createPoster(Film film, String posterFilePath) {
        Poster poster = Poster.builder()
                .id(null)
                .filePath(posterFilePath)
                .build();
        poster.setFilm(film);
        posterRepository.save(poster);
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void readFilmByNameAndGenre() {
        List<Film> films = filmRepository.findAll();
        for (Film film : films) {
            log.warn("Film name {}", film.getName());
            log.warn("Film name {}", film.getGenreType());
        }
    }

    @Override
    public Collection<Film> findByActor(String actor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public JpaRepository<Film, Long> getRepository() {
        return filmRepository;
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void updateFilmAndMarathon(Long id, String filmName, LocalDateTime marathonStartTime) {
        Film film = filmRepository.findById(id).orElseThrow();
        film.setName(filmName);

        if (CollectionUtils.isNotEmpty(film.getMarathons())) {
            log.info("Previous film name: {}", film.getMarathons().get(0).getName());
            marathonService.updateMarathonStartTime(film.getMarathons().get(0).getId(), marathonStartTime);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateMarathonStartTime(Long marathonId, LocalDateTime marathonStartTime) {
        try {
            Marathon marathon = marathonRepository.findById(marathonId).orElseThrow();
            marathon.setStartTime(marathonStartTime);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }

}
