package com.example.cinema.services.map;

import com.example.cinema.entity.domain.BaseEntity;
import com.example.cinema.services.BaseMapService;
import com.example.cinema.services.CrudService;

import java.util.Collection;

public abstract class AbstractMapService<T extends BaseEntity<ID>,ID> implements CrudService<T,ID>, BaseMapService<T,ID> {
    @Override
    public T findById(ID id) {
        return getRecourse().get(id);
    }

    @Override
    public void save(T entity) {
        getRecourse().put(entity.getId(), entity);
    }

    @Override
    public Collection<T> findAll() {
        return getRecourse().values();
    }

    @Override
    public void delete(ID id) {
        getRecourse().remove(id);
    }
}
