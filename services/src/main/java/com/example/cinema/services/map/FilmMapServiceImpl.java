package com.example.cinema.services.map;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.enumEntity.GenreType;
import com.example.cinema.entity.domain.Film;
import com.example.cinema.services.FilmService;
import com.example.cinema.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MapImplementation
public class FilmMapServiceImpl extends AbstractMapService<Film, Long> implements FilmService {
    private static final Map<Long, Film> resource = new HashMap<>();

    @Override
    public Map<Long, Film> getRecourse() {
        return resource;
    }

    @Override
    public Long createFilm(String name, String description, GenreType genreType, Double length, Integer requiredAge,String posterFilePath) {
        return null;
    }

    @Override
    public Collection<Film> findByActor(String actor) {
        return null;
    }


}
