package com.example.cinema.services;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Ticket;
import com.example.cinema.entity.domain.dto.MarathonDto;
import com.example.cinema.entity.domain.dto.TicketDto;
import com.example.cinema.services.CrudService;

import java.math.BigDecimal;
import java.util.List;

public interface TicketService extends CrudService<Ticket, Long> {
    void findByPrice(String price);

    default List<Ticket> findTicket(TicketDto ticketDto) {
        throw new UnsupportedOperationException();
    }

}
