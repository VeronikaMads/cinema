package com.example.cinema.services.map;

import com.example.cinema.entity.domain.Session;
import com.example.cinema.services.SessionService;
import com.example.cinema.services.config.MapImplementation;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class SessionMapServiceImpl extends AbstractMapService<Session, Long> implements SessionService {
    private static final Map<Long, Session> resource = new HashMap<>();

    @Override
    public Map<Long, Session> getRecourse() {
        return resource;
    }

    @Override
    public void sessionFindByDataTime(LocalDate dataTime) {

    }
}


