package com.example.cinema.services.jpa;

import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.Ticket;
import com.example.cinema.entity.domain.dto.SessionDto;
import com.example.cinema.entity.domain.dto.TicketDto;
import com.example.cinema.mapper.TicketMapper;
import com.example.cinema.repository.TicketRepository;
import com.example.cinema.services.TicketService;
import com.example.cinema.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class TicketJpaServiceImpl extends AbstractJpaService<Ticket, Long> implements TicketService {
    private final TicketRepository ticketRepository;
    TicketMapper mapper;

    public Long createTicket(String seat, BigDecimal price) {
        List<Ticket> tickets = new ArrayList<Ticket>();
        Ticket ticket = Ticket.builder()
                .id(null)
                .seat(seat)
                .price(price)
                .build();
        ticketRepository.save(ticket);
        return ticket.getId();
    }

    @Override
    public JpaRepository<Ticket, Long> getRepository() {
        return ticketRepository;
    }

    @Override
    public void findByPrice(String price) {
        List<Ticket> tickets = ticketRepository.findAll();
        for (Ticket ticket : tickets) {
            log.warn("Ticket price: ", ticket.getPrice());
        }
    }

    public List<Ticket> findTicket(TicketDto ticketDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Ticket> example = Example.of(mapper.map(ticketDto),caseInsensitiveExMatcher);
        return ticketRepository.findAll(example);

    }

}
