package com.example.cinema.services;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.dto.MarathonDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MarathonService extends CrudService<Marathon,Long>{

    void updateMarathonStartTime(Long marathonId, LocalDateTime marathonStartTime);

    default List<Marathon> findMarathon(MarathonDto marathonDto) {
        throw new UnsupportedOperationException();
    }

}
