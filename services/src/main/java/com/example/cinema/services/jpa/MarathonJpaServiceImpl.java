package com.example.cinema.services.jpa;

import com.example.cinema.entity.domain.Film;
import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.dto.MarathonDto;
import com.example.cinema.entity.exception.EntityDoesNotExistsException;
import com.example.cinema.mapper.MarathonMapper;
import com.example.cinema.repository.FilmRepository;
import com.example.cinema.repository.MarathonRepository;
import com.example.cinema.services.MarathonService;
import com.example.cinema.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class MarathonJpaServiceImpl extends AbstractJpaService<Marathon, Long> implements MarathonService {
    private final MarathonRepository marathonRepository;
    private final FilmRepository filmRepository;
    private final MarathonMapper mapper;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long createMarathon(String name, LocalDateTime startTime, List<Long> filmIds) {
        List<Film> films = new ArrayList<Film>();
        for (Long filmId : filmIds) {
            Optional<Film> filmOptional = filmRepository.findById(filmId);
            if (!filmOptional.isPresent()) { //isPresent()/isEmpty - Присутствует какое-либо значение
                throw new EntityDoesNotExistsException("Film id:" + filmId);
            }
            films.add(filmOptional.get());
        }

        Marathon marathon = Marathon.builder()
                .id(null)
                .name(name)
                .startTime(startTime)
                .build();
        marathon.setFilms(films);
        marathonRepository.save(marathon);
        return marathon.getId();

    }

    @Override
    public JpaRepository<Marathon, Long> getRepository() {
        return marathonRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void updateMarathonStartTime(Long marathonId, LocalDateTime marathonStartTime) {
        Marathon marathon = marathonRepository.findById(marathonId).orElseThrow();
        marathon.setStartTime(marathonStartTime);

    }

    @Override
    public List<Marathon> findMarathon(MarathonDto marathonDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Marathon> example = Example.of(mapper.map(marathonDto),caseInsensitiveExMatcher);
        return marathonRepository.findAll(example);
    }
}
