package com.example.cinema.services.jpa;

import com.example.cinema.entity.domain.Film;
import com.example.cinema.entity.domain.Hall;
import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.dto.SessionDto;
import com.example.cinema.entity.exception.EntityDoesNotExistsException;
import com.example.cinema.mapper.SessionMapper;
import com.example.cinema.repository.FilmRepository;
import com.example.cinema.repository.HallRepository;
import com.example.cinema.repository.SessionRepository;
import com.example.cinema.services.SessionService;
import com.example.cinema.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class SessionJpaServiceImpl extends AbstractJpaService<Session, Long> implements SessionService {
    private final SessionRepository sessionRepository;
    private final HallRepository hallRepository;
    private final FilmRepository filmRepository;
    private final SessionMapper mapper;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long createSession(LocalDateTime startTime, Long hallId, Long filmId) {
        Optional<Hall> hallOptional = hallRepository.findById(hallId);
        if (!hallOptional.isPresent()) {
            throw new EntityDoesNotExistsException("Hall id: " +hallId);
        }
        Optional<Film> filmOptional = filmRepository.findById(filmId);
        if (!filmOptional.isPresent()){
            throw new EntityDoesNotExistsException("Film id: " +filmId);
        }
        Session session = Session.builder()
                .id(null)
                .startTime(startTime)
                .build();
        session.setHall(hallOptional.get());
        session.setFilm(filmOptional.get());

        sessionRepository.save(session);
        return session.getId();

    }

    @Override
    public JpaRepository<Session, Long> getRepository() {
        return sessionRepository;
    }


    @Override
    public void sessionFindByDataTime(LocalDate dataTime) {
        List<Session> sessions = sessionRepository.findAll();
        for(Session session:sessions){
            log.warn("Session data: ", session.getStartTime());
        }
    }

    @Override
    public List<Session> findSession(SessionDto sessionDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Session> example = Example.of(mapper.map(sessionDto),caseInsensitiveExMatcher);
        return sessionRepository.findAll(example);

    }
}

