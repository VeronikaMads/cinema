package com.example.cinema.services;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.dto.MarathonDto;
import com.example.cinema.entity.domain.dto.SessionDto;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

public interface SessionService extends CrudService<Session, Long> {

    void sessionFindByDataTime(LocalDate dataTime);

    default List<Session> findSession(SessionDto sessionDto) {
        throw new UnsupportedOperationException();
    }

}
