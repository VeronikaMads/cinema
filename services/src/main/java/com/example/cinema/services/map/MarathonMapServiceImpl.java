package com.example.cinema.services.map;

import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Session;
import com.example.cinema.services.MarathonService;
import com.example.cinema.services.config.MapImplementation;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@MapImplementation
public class MarathonMapServiceImpl extends AbstractMapService<Marathon,Long> implements MarathonService {
    private static final Map<Long, Marathon> resource = new HashMap<>();
    @Override
    public Map<Long, Marathon> getRecourse() {
        return resource;
    }

    @Override
    public void updateMarathonStartTime(Long marathonId, LocalDateTime marathonStartTime) {

    }
}
