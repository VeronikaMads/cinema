package com.example.cinema.services;

import com.example.cinema.entity.domain.Film;
import com.example.cinema.entity.enumEntity.GenreType;

import java.util.Collection;


public interface FilmService extends CrudService<Film, Long> {
    Long createFilm(String name, String description, GenreType genreType, Double length, Integer requiredAge, String posterFilePath);

    Collection<Film> findByActor(String actor);


}
