package com.example.cinema.repository;

import com.example.cinema.entity.domain.TicketSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TicketSessionRepository extends JpaRepository<TicketSession,Long> {
    List<TicketSession> findByStartTime(LocalDateTime StartTime);
}
