package com.example.cinema.repository;

import com.example.cinema.entity.domain.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ActorRepository extends PersonRepository<Actor,Long> {

}
