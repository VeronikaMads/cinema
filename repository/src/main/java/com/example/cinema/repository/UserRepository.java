package com.example.cinema.repository;
import com.example.cinema.entity.inside.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends PersonRepository<User,Long> {

    Optional<User> findByLogin(String login);


}
