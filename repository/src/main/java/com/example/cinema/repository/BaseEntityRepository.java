package com.example.cinema.repository;

import com.example.cinema.entity.domain.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface BaseEntityRepository<T extends BaseEntity<?>, ID> extends JpaRepository<T, ID> {

    List<T> findByCreatedBy(String CreatedBy);
}
