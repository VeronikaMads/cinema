package com.example.cinema.repository;

import com.example.cinema.entity.domain.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface SessionRepository extends JpaRepository<Session, Long> {
}
