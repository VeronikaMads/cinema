package com.example.cinema.repository;

import com.example.cinema.entity.inside.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
@NoRepositoryBean
public interface PersonRepository<T extends Person,ID> extends JpaRepository<T,ID> {
    List<T> findByLastName(String lastName);
    List<T> findByFirstName(String firstName);
    List<T> findByFullName(String fullName);
}
