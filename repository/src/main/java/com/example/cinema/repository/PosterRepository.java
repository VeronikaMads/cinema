package com.example.cinema.repository;

import com.example.cinema.entity.domain.Poster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PosterRepository extends JpaRepository<Poster,Long> {
}
