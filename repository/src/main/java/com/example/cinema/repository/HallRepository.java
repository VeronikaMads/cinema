package com.example.cinema.repository;

import com.example.cinema.entity.domain.Hall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HallRepository extends JpaRepository<Hall,Long> {
}
