package com.example.cinema.repository;

import com.example.cinema.entity.inside.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permissions,Long> {

    Optional<Permissions> findByName(String Name);
}

