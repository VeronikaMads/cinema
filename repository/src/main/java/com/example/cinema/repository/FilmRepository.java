package com.example.cinema.repository;

import com.example.cinema.entity.domain.Actor;
import com.example.cinema.entity.domain.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FilmRepository extends JpaRepository<Film,Long> {
    List<Film> findByCreatedByAndName(String createdBy, String Name);

    @Query("SELECT f FROM Film f WHERE f.name like: filmName")
    Optional<Film> findByName(@Param("filmName") String name);

    @Query(value = "SELECT a FROM Actor a WHERE a.fullName like: name or a.films.genreType like: genreType" ,nativeQuery = true)
    Optional<Film> findByActorsOrGenreType(@Param("name") String name, @Param("genreType") String genreType);
}
