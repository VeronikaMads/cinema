package com.example.cinema.repository;

import com.example.cinema.entity.domain.Marathon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarathonRepository extends JpaRepository<Marathon,Long> {
}
