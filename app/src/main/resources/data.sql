INSERT INTO hall (id,name,capacity,description) values ('61','Hall 1','100','10 rows of 10 seats');
INSERT INTO hall (id,name,capacity,description) values ('62','Hall 2','400','20 rows of 20 seats');
INSERT INTO hall (id,name,capacity,description) values ('63','Hall 3','900','30 rows of 30 seats');
INSERT INTO hall (id,name,capacity,description) values ('64','Hall 4','1600','40 rows of 40 seats');

INSERT INTO permissions (id,name) values ('32','update_user');
INSERT INTO permissions (id,name) values ('33','delete_user');
INSERT INTO permissions (id,name) values ('34','create_session');
INSERT INTO permissions (id,name) values ('35','delete_session');

INSERT INTO marathon (id,created_at,last_modified_at,name,start_time) values ('36','2021-11-01','2021-12-02','Feature films marathon','2021-10-21 16:30:00');
INSERT INTO marathon (id,created_at,last_modified_at,name,start_time) values ('37','2021-11-01','2021-12-02','Сomic strip movies marathon','2021-02-12 16:00:00');
INSERT INTO marathon (id,created_at,last_modified_at,name,start_time) values ('38','2021-11-01','2021-12-02','film marathon directed by Quentin Jerome Tarantino','2021-06-20 16:30:00');

INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('39','2021-11-01','2021-12-02','Kurchin','Ivan','Kurchin Ivan','ivanov392487356','123','MANAGER','Minsk','Karl Marx st.','+375295632456','ivanov@gmail.com');
INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('40','2021-11-01','2021-12-02','Sidorov','Nikolay','Sidorov Nikolay','sidorov584526','456','MANAGER','Minsk','Lenin st.','+375298092425','sidorov@gmail.com');
INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('41','2021-11-01','2021-12-02','Gritsuk','Katia','Katia Gritsuk','Katia735655','789','CASHIER','Minsk','Kiseleva st.','+375298524799','Katia@gmail.com');
INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('42','2021-11-01','2021-12-02','Vartas','lyubov','lyubov Vartas','Luba6','101','CASHIER','Minsk','Zybitskaya st','+375294782536','Luba6@gmail.com');
INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('43','2021-11-01','2021-12-02','Barat','Ivan','Ivan Barat','ivan87356','123','CASHIER','Minsk','Soviet st.','+375298056635','ivan@gmail.com');
INSERT INTO user (id,created_at,last_modified_at,last_name,first_name,full_name,login,password,position,city,street,phone,mail) values ('44','2021-11-01','2021-12-02','Nikolaev','Dmitriy','Dmitriy Nikolaev','dima356','145','CASHIER','Minsk','Kiseleva st','+375299634545','dima@gmail.com');

INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('25','2021-11-01','2021-12-02','Bogart','Humphrey','Humphrey Bogart');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('26','2021-10-01','2021-12-03','Hepburn','Katharine','Katharine Hepburn');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('27','2021-09-01','2021-12-04','Bet','Cary','Cary Grant Bet');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('28','2021-08-01','2021-12-05','Stewart','James','James Stewart');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('29','2021-07-01','2021-12-06','Hepburn','Audrey','Audrey Hepburn');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('30','2021-06-01','2021-12-07','Marlon','Brando','Marlon Brando');
INSERT INTO actor (id,created_at,last_modified_at,last_name,first_name,full_name) values ('31','2021-05-01','2021-12-08','Bergman','Ingrid','Ingrid Bergman');

INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('5','2021-12-01','2021-12-01','Cold Mountain','If hate sends men to war,then it must be love that brings them home','DRAMA',106,18);
INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('6','2021-12-01','2021-12-01','12 Years a Slave','The extraordinary true story of Solomon Northup','DRAMA',133,16);
INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('7','2021-12-01','2021-12-01','No Country for Old Men','You cant get out of the water dry','DETECTIVE',122,16);
INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('8','2021-12-01','2021-12-01','One Flew Over the Cuckoo-s Nest','If hes crazy,what does that make you?','DRAMA',108,18);
INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('9','2021-12-01','2021-12-01','Around the World in 80 Days','See everything in the World worth seeing!Do everything in the World worth doing!','COMEDY',167,0);
INSERT INTO film (id,created_at,last_modified_at,name,description,genre_type,length,required_age) values ('10','2021-12-01','2021-12-01','Tom Jones','The World-s Fair-Haired Boy','COMEDY',129,0);

INSERT INTO poster (id,film_id,file_path) values ('11','5','/images/posterColdMountain.png');
INSERT INTO poster (id,film_id,file_path) values ('12','6','/images/poster12YearsASlave.png');
INSERT INTO poster (id,film_id,file_path) values ('13','7','/images/posterNoCountryForOldMen.png');
INSERT INTO poster (id,film_id,file_path) values ('14','8','/images/posterOneFlewOverTheCuckoo-sNest.png');
INSERT INTO poster (id,film_id,file_path) values ('15','9','/images/posterAroundTheWorldIn80Days.png');
INSERT INTO poster (id,film_id,file_path) values ('16','10','/images/posterTomJones.png');

INSERT INTO session (id,created_at,last_modified_at,film_id,hall_id,start_time) values ('17','2021-12-01','2021-12-01','7','61','2021-10-21 16:30:00');
INSERT INTO session (id,created_at,last_modified_at,film_id,hall_id,start_time) values ('18','2021-12-01','2021-12-01','8','62','2021-05-02 15:00:00');
INSERT INTO session (id,created_at,last_modified_at,film_id,hall_id,start_time) values ('19','2021-12-01','2021-12-01','5','63','2021-10-21 21:30:00');
INSERT INTO session (id,created_at,last_modified_at,film_id,hall_id,start_time) values ('20','2021-12-01','2021-12-01','9','64','2021-10-21 21:30:00');

INSERT INTO ticket (id,created_at,last_modified_at,session_id,seat,price) values ('21','2021-12-01','2021-10-21 16:30:00','20','r10m2',21.00);
INSERT INTO ticket (id,created_at,last_modified_at,session_id,seat,price) values ('22','2021-12-01','2021-10-21 16:30:00','18','r8m1',20.00);
INSERT INTO ticket (id,created_at,last_modified_at,session_id,seat,price) values ('23','2021-12-01','2021-10-21 16:30:00','19','r8m2',19.00);
INSERT INTO ticket (id,created_at,last_modified_at,session_id,seat,price) values ('24','2021-12-01','2021-10-21 16:30:00','19','r5m11',18.00);

CREATE VIEW ticket_session (session_id, start_time, seat, price, film_name, hall_name) AS SELECT s.ID, s.START_TIME, t.SEAT, t.PRICE, f.NAME, h.NAME FROM SESSION s, TICKET t, FILM f, HALL h WHERE s.ID = t.SESSION_ID and s.FILM_ID = f.ID and s.HALL_ID = h.ID;
CREATE VIEW marathon_actor(marathon_id, marathon_name, marathon_start, actor_full_name, film_name, genre_type_film, description_film, length_film, age_film) AS SELECT m.ID, m.NAME, m.START_TIME, a.FULL_NAME, f.NAME, f.GENRE_TYPE, f.DESCRIPTION, f.LENGTH, f.REQUIRED_AGE FROM MARATHON m, MARATHON_FILM mf, ACTOR a, ACTOR_FILM af, FILM f WHERE m.ID = mf.MARATHON_ID and mf.MARATHON_ID = f.ID and af.FILM_ID = f.ID and af.FILM_ID = a.ID;