package com.example.cinema.api;

import com.example.cinema.config.Runner;
import com.example.cinema.entity.domain.Marathon;
import com.example.cinema.entity.domain.Session;
import com.example.cinema.entity.domain.Ticket;
import com.example.cinema.entity.domain.TicketSession;
import com.example.cinema.entity.domain.dto.MarathonDto;
import com.example.cinema.entity.domain.dto.SessionDto;
import com.example.cinema.entity.domain.dto.TicketDto;
import com.example.cinema.repository.TicketRepository;
import com.example.cinema.repository.TicketSessionRepository;
import com.example.cinema.services.MarathonService;
import com.example.cinema.services.SessionService;
import com.example.cinema.services.TicketService;
import com.example.system.entity.SystemOption;
import com.example.system.repository.SystemRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
@AllArgsConstructor
@ComponentScan(basePackages = {"com.example.cinema.repository","com.example.system.repository"})
@RequestMapping("/")
@RestController
public class TestController {
    private final TicketSessionRepository ticketSessionRepository;
    private final TicketRepository ticketRepository;
    private final TicketService ticketService;
    private final SessionService sessionService;
    private final MarathonService marathonService;
    private final SystemRepository systemRepository;

    private final Runner runner;


    @GetMapping("/view")
    @Transactional
    public List<TicketSession> getView(){
        return ticketSessionRepository.findAll();
    }
    @GetMapping("/system")
    public List<SystemOption> getSystem(){
        return systemRepository.findAll();
    }

    @ApiOperation(value = "Get all tickets", response = Ticket.class, responseContainer = "List")
    @GetMapping("/tickets")
    public List<Ticket> getTickets(){
        return ticketRepository.findAll();
    }

    @ApiOperation(value = "Find tickets", response = Ticket.class, responseContainer = "List")
    @PutMapping("/tickets/find")
    public List<Ticket> findTickets(@ApiParam(value = "Search example") @RequestBody TicketDto ticketDto) {
        return ticketService.findTicket(ticketDto);
    }

    @ApiOperation(value = "Find session", response = Session.class, responseContainer = "List")
    @PutMapping("/session/find")
    public List<Session> findSessions(@ApiParam(value = "Search example") @RequestBody SessionDto sessionDto) {
        return sessionService.findSession(sessionDto);
    }

    @ApiOperation(value = "Find marathon", response = Marathon.class, responseContainer = "List")
    @PutMapping("/marathon/find")
    public List<Marathon> findMarathons(@ApiParam(value = "Search example") @RequestBody MarathonDto marathonDto) {
        return marathonService.findMarathon(marathonDto);
    }
}
