package com.example.cinema;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@ComponentScan({"com.example.cinema.repository"})
//@EntityScan("com.example.cinema.entity")
//@EnableJpaRepositories("com.example.cinema.repository")
//@ComponentScan({"com.example.cinema.entity"})
@SpringBootApplication
public class CinemaApplication{
    public static void main(String[] args) {
        SpringApplication.run(CinemaApplication.class, args);

    }
}
