package com.example.cinema.config;

import com.example.cinema.DefaultProperties;
import com.example.cinema.entity.domain.Film;
import com.example.cinema.entity.enumEntity.GenreType;
import com.example.cinema.repository.FilmRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("MainRunner")
public class Runner implements CommandLineRunner {
    @Autowired
    DefaultProperties defaultProperties;

    @Override
    public void run(String... args) throws Exception {
        System.out.println(defaultProperties.getFilms());

    }

    }

