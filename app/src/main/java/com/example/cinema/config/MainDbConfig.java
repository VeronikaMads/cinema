package com.example.cinema.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
@EnableJpaRepositories(
        entityManagerFactoryRef = "mainEntityManagerFactory",
        basePackages = {"com.example.cinema.repository"}
)
@Configuration
public class MainDbConfig {
    @Primary
    @LiquibaseDataSource
    @ConfigurationProperties("spring.datasource")
    @Bean("mainDataSource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().type(ManagedHikariDataSource.class).build();

    }
    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean mainEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("mainDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .packages("com.example.cinema.entity")
                .persistenceUnit("mainPersistenceUnit")
                .build();
    }
    @Primary
    @Bean
    public PlatformTransactionManager transactionManager(@Qualifier("mainEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
