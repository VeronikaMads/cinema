package com.example.cinema.controller;

import com.example.cinema.services.SessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RequestMapping("/cinema/session")
@RequiredArgsConstructor
@RestController
public class SessionController {
    private final SessionService sessionService;
}
