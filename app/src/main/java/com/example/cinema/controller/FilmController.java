package com.example.cinema.controller;

import com.example.cinema.services.FilmService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/cinema/film")
@RequiredArgsConstructor
@RestController
public class FilmController {
    private final FilmService filmService;
}
