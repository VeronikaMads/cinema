package com.example.cinema.entity.domain;

import com.example.cinema.entity.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@View
@Table(name = "MARATHON_ACTOR")
public class MarathonActor {
    @Id
    private Long marathonId;
    @Column(name = "marathonName")
    private String marathonName;
    @Column(name = "marathonStart")
    private LocalDateTime marathonStart;
    @Column(name = "actorFullName")
    private String actorFullName;
    @Column(name = "filmName")
    private String filmName;
    @Column(name = "genreTypeFilm")
    private String genreTypeFilm;
    @Column(name = "descriptionFilm")
    private String descriptionFilm;
    @Column(name = "lengthFilm")
    private Double lengthFilm;
    @Column(name = "ageFilm")
    private Integer ageFilm;

}
