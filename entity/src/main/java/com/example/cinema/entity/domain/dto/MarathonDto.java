package com.example.cinema.entity.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Data
public class MarathonDto {
    private Long id;
    private String name;
    private LocalDateTime startTime;
    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;
}
