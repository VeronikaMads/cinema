package com.example.cinema.entity.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Session extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY) //SEQUENCE
    private Long id;
    private LocalDateTime startTime;
    @OneToMany(mappedBy = "session")
    private List<Ticket> tickets;
    @JoinColumn(name = "hall_id")
    @ManyToOne
    private Hall hall;
    @JoinColumn(name = "film_id")
    @ManyToOne
    private Film film;

}
