package com.example.cinema.entity.enumEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public enum Position {

    MANAGER(Values.MANAGER),
    CASHIER(Values.CASHIER);

    private static final Map<String, Position> MAP = Arrays.stream(Position.values())
            .collect(Collectors.toMap(Position::getValue, Function.identity()));
    private String value;

    public static Position getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return MAP.get(value);

    }

    public static class Values {
        public final static String MANAGER = "Manager";
        public final static String CASHIER = "Cashier";
    }

}
