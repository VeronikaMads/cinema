package com.example.cinema.entity.converter;

import com.example.cinema.entity.enumEntity.GenreType;
import org.springframework.stereotype.Component;
import javax.persistence.AttributeConverter;
@Component
public class GenreTypeConverter implements AttributeConverter<GenreType,String> {


    @Override
    public String convertToDatabaseColumn(GenreType attribute) {
        return attribute.getValue();
    }

    @Override
    public GenreType convertToEntityAttribute(String dbData) {
        return GenreType.getByValue(dbData);
    }
}
