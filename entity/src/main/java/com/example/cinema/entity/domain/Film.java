package com.example.cinema.entity.domain;

import com.example.cinema.entity.enumEntity.GenreType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Film extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private GenreType genreType;
    private Double length;
    private Integer requiredAge;
    @ManyToMany(mappedBy = "films",cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    private List<Marathon> marathons;
    @ManyToMany(mappedBy = "films", cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    private List<Actor> actors;

}
