package com.example.cinema.entity.enumEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public enum GenreType {

    HORROR(Values.HORROR),

    ACTION(Values.ACTION),

    WESTERN(Values.WESTERN),

    DETECTIVE(Values.DETECTIVE),

    DRAMA(Values.DRAMA),

    HISTORICAL(Values.HISTORICAL),

    COMEDY(Values.COMEDY),

    MELODRAMA(Values.MELODRAMA);

    private static final Map<String, GenreType> MAP = Arrays.stream(GenreType.values())
            .collect(Collectors.toMap(GenreType::getValue, Function.identity()));
    private String value;

    public static GenreType getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return MAP.get(value);

    }

    public static class Values {
        public final static String HORROR = "Horror";
        public final static String ACTION = "Action";
        public final static String WESTERN = "Western";
        public final static String DETECTIVE = "Detective";
        public final static String DRAMA = "Drama";
        public final static String HISTORICAL = "Historical";
        public final static String COMEDY = "Comedy";
        public final static String MELODRAMA = "Melodrama";

    }
}
