package com.example.cinema.entity.inside;

import com.example.cinema.entity.converter.PositionConverter;
import com.example.cinema.entity.enumEntity.Position;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class User extends Person {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    @Convert(converter = PositionConverter.class)
    private Position position;
    @JoinTable(name = "user_perm",
            joinColumns = @JoinColumn(name = "usr_id"),
            inverseJoinColumns = @JoinColumn(name = "perm_id"))
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    private Set<Permissions> permissions;
    @Embedded
    private ContactDetails additional;
}
