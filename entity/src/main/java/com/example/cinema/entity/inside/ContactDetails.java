package com.example.cinema.entity.inside;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Embeddable
public class ContactDetails {
    private String phone;
    private String mail;
    private String street;
    private String city;
}
