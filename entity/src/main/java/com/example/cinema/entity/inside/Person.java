package com.example.cinema.entity.inside;
import com.example.cinema.entity.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@MappedSuperclass
public abstract class Person extends BaseEntity<Long> {
    private String firstName;
    private String lastName;
    private String fullName;

    @PrePersist
    @PreUpdate
    private void initFullName(){
        this.fullName= getFirstName()+" "+ getLastName();
    }
}
