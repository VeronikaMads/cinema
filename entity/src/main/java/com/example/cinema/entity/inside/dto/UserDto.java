package com.example.cinema.entity.inside.dto;

import com.example.cinema.entity.converter.PositionConverter;
import com.example.cinema.entity.enumEntity.Position;
import lombok.Data;

import javax.persistence.Convert;
import java.time.OffsetDateTime;

@Data
public class UserDto {
    private String firstName;
    private String lastName;
    private String fullName;
    private Long id;
    private String login;
    private String password;
    private Position position;
    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;

}
