package com.example.cinema.entity.converter;

import com.example.cinema.entity.enumEntity.Position;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class PositionConverter implements AttributeConverter<Position, String> {
    @Override
    public String convertToDatabaseColumn(Position attribute) {
        return attribute.getValue();
    }

    @Override
    public Position convertToEntityAttribute(String dbData) {
        return Position.getByValue(dbData);
    }
}
