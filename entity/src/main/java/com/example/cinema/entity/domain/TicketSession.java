package com.example.cinema.entity.domain;

import com.example.cinema.entity.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
//@Setter no update!!
@Getter
@View
@Entity
@Table(name = "TICKET_SESSION")
public class TicketSession  {
    @Id
    private Long sessionId;
    @Column(name = "startTime")
    private LocalDateTime startTime;
    @Column(name = "seat")
    private String seat;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "filmName")
    private String filmName;
    @Column(name = "hallName")
    private String hallName;

}
