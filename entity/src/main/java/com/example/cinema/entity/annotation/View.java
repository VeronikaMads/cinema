package com.example.cinema.entity.annotation;

import org.hibernate.annotations.Immutable;

@Immutable
public @interface View {
}
