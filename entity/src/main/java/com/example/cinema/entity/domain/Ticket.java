package com.example.cinema.entity.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Ticket extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) //SEQUENCE
    private Long id;
    private String seat;
    private BigDecimal price;

    @JoinColumn(name = "session_id")
    @ManyToOne
    private Session session;
}
