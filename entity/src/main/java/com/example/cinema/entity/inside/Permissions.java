package com.example.cinema.entity.inside;

import lombok.*;

import javax.persistence.*;
import java.util.List;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Permissions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToMany(mappedBy = "permissions",cascade = {CascadeType.DETACH,CascadeType.REFRESH})
    private List<User> users;
}
